#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  passwd.py
#  
#  Copyright 2012 Said Babayev <tesaid@azercell.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import hashlib
import getpass
import pickle
import sys
import os

global startpath
startpath = os.path.dirname(sys.argv[0])

class AuthBase(object):
    def __init__(self):
        self.lsgroup = '/etc/lightsquid/group.cfg'
        #self.lsgroup = 'group.cfg'
    def read(self):
        #username:pwd
        #Generic auth
        try:
            print startpath + '/passwd'
            f = open(startpath + '/passwd')
            self.passwd = pickle.load(f)
            f.close()
        except:
            self.passwd = {}
    
    def readgroups(self):
        #username:[groups]
        #Which users have access to groups
        try:
            f = open(startpath + '/groupaccess')
            self.groups = pickle.load(f)
            f.close()
        except:
            self.groups = {}
    
    def readlsgroup(self):
        #Read groups from lightsquid config and present in id:groupname
        try:
            f = open(self.lsgroup)
            l = f.readlines()
            f.close()
        except:
            print 'Lightsquid group.cfg not found!'
            return 1
        #split records
        for i in range(len(l)):
            l[i] = l[i].split('\t')
        #id:groupname
        self.lsgroups = {}
        for item in l:
            self.lsgroups[int(item[1])] = item[2].strip()
            
    def checkUserGroup(self, user, group):
        #Check target user (squid user) in group
        try:
            f = open(self.lsgroup)
            l = f.readlines()
            f.close()
        except:
            print 'Lightsquid group.cfg not found!'
            return 1
        #split records
        for i in range(len(l)):
            l[i] = l[i].split('\t')
        #search and match
        for item in l:
            if item[0] == user and item[2].strip() in group:
                return True
        return False
        print user, group

class AuthMgr(AuthBase):
    def __init__(self):
        super(AuthMgr, self).__init__()
        self.read()
        self.readgroups()
        self.readlsgroup()
    
    def echo(self):
        print 'Usernames: ', self.passwd
        print 'Group access: ', self.groups
    
    def write(self):
        #create / update user/password
        username = raw_input('Enter username: ')
        password = getpass.getpass()
        md = hashlib.md5()
        md.update(password)
        password = md.hexdigest()
        self.passwd[username] = password
        f = open(startpath + '/passwd', 'w')
        pickle.dump(self.passwd, f)
        f.close()
    
    def deleteUser(self):
        #Delete user
        username = raw_input('Enter username: ')
        if username in self.passwd:
            try:
                gr = self.groups[username]
                print 'Username has access to following groups:', gr
            except KeyError:
                print 'User has no access to any groups'
            ans = raw_input('Are you sure you want to delete {0}? (y/n)'.format(username))
            if ans in ['y','yes', 'Y', 'Yes']:
                del self.passwd[username]
                try:
                    del self.groups[username]
                except:
                    pass
                f = open(startpath + '/groupaccess', 'w')
                pickle.dump(self.groups, f)
                f.close()
                f = open(startpath + '/passwd', 'w')
                pickle.dump(self.passwd, f)
                f.close()
            else:
                print 'User is NOT deleted'
        else:
            print 'Username not found'
        
    def writegroups(self):
        #Write user's group access info
        username = raw_input('Enter username: ')
        #check if username exists
        if not username in self.passwd:
            print 'Username does not exist'
            return 1
        #return list of available groups
        for item in self.lsgroups.items():
            print 'ID: {0}, Name: {1}'.format(item[0], item[1])
        a = raw_input('Select an ID to add: ')
        if not username in self.groups:
            self.groups[username] = []
        if not int(a) in self.groups[username]:
            self.groups[username].append(int(a))
        f = open(startpath + '/groupaccess', 'w')
        pickle.dump(self.groups, f)
        f.close()
    
    def deletegroups(self):
        #Delete user's access from group
        username = raw_input('Enter username: ')
        #check if username exists
        if not username in self.passwd:
            print 'Username does not exist'
            return 1
        for item in self.groups[username]:
            try:
                grid = self.lsgroups[item]
            except:
                grid = None
            print 'ID: {0}, Name: {1}'.format(item, grid)
        a = raw_input('Select an ID to delete: ')
        if not username in self.groups:
            self.groups[username] = []
        if int(a) in self.groups[username]:
            ind = self.groups[username].index(int(a))
            self.groups[username].pop(ind)
        f = open(startpath + '/groupaccess', 'w')
        pickle.dump(self.groups, f)
        f.close()

def main():
    if not len(sys.argv) == 2:
        print 'Use following format:\n {0} <command>\n Commands could be: write, delete, group, deletegroup, echo'.format(sys.argv[0])
        return 1
    else:
        a = AuthMgr()
        if sys.argv[1] == 'write':
            a.write()
        elif sys.argv[1] == 'group':
            a.writegroups()
        elif sys.argv[1] == 'deletegroup':
            a.deletegroups()
        elif sys.argv[1] == 'echo':
            a.echo()
        elif sys.argv[1] == 'delete':
            a.deleteUser()
        else:
            print 'Wrong command'
    return 0

if __name__ == '__main__':
    main()

