#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  lightsquid-access.py
#  
#  Copyright 2012 Said Babayev <tesaid@azercell.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import pickle
import hashlib
import base64
import uuid
import sys
import tornado.web
import tornado.ioloop
from passwd import AuthBase
from parser import Opener

global keys
global serverurl
global version
keys = ['year', 'month', 'day', 'user', 'mode']
serverurl = 'http://syslogd.azercell.com/lightsquid/'
version = '1.7'

class Auth(AuthBase):
    def check(self, username, password):
        #check if username is in list
        self.read()
        if username in self.passwd:
            #check if password is correct
            md = hashlib.md5()
            md.update(password)
            password = md.hexdigest()
            if self.passwd[username] == password:
                return True
            else:
                #Password incorrect
                return False
        else:
            #Username not defined
            return False
    
    def getGroups(self, username):
        #return name of groups where user can access
        self.readgroups()
        self.readlsgroup()
        groupnames = []
        for item in self.groups[username]:
            groupnames.append(self.lsgroups[item])
        return groupnames
        
class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")
    
    def genericRender(self, url, rtable, dic=None, groups=None, mode=None):
        o = Opener()
        ans = o.fetch(url, rtable, dic, groups, mode)
        self.write(ans)
    
    def getArgs(self):
        if len(self.request.arguments) > 0:
            #resulting list if argument pairs
            self.pairs = []
            #Fetch pair of arg=value keys
            pair = {}
            #extract values from lists
            for k in keys:
                if len(self.get_arguments(k)) > 0:
                    pair[k] = self.get_arguments(k)[0]
                    if not pair in self.pairs:
                        self.pairs.append(pair)
        
class MainHandler(BaseHandler):
    def get(self):
        #Check if current_user has a value
        if not self.current_user:
            #If not, redirect him to login page
            self.redirect("/login")
            return
        name = tornado.escape.xhtml_escape(self.current_user)
        #Auth/cookie is ok, proceed with group access fetching
        self.genericRender(serverurl, True)

class IndexCGIHandler(BaseHandler):
    def get(self, *args):
        #Check if current_user has a value
        if not self.current_user:
            #If not, redirect him to login page
            self.redirect("/login")
            return
        #proceed with fetching
        self.getArgs()
        if len(self.pairs) > 0:
            #index.cgi, ind = 1
            ind = 1
            self.genericRender(serverurl + args[0] + '?', ind, self.pairs[0])
        else:
            #something wrong
            return 1

class GroupCGIHandler(BaseHandler):
    def get(self, *args):
        #Check if current_user has a value
        if not self.current_user:
            #If not, redirect him to login page
            self.redirect("/login")
            return
        #proceed with fetching
        self.getArgs()
        #Get groups
        g = Auth()
        self.groupnames = g.getGroups(self.current_user)
        if len(self.pairs) > 0:
            #group.cgi ind = 2
            ind = 2
            if 'mode' in self.pairs[0]:
                #Consider mode
                self.genericRender(serverurl + args[0] + '?', ind, self.pairs[0], self.groupnames, True)
            else:
                self.genericRender(serverurl + args[0] + '?', ind, self.pairs[0], self.groupnames)
        else:
            #something wrong
            return 1

class UserCGIHandler(BaseHandler):
    def get(self, *args):
        #Check if current_user has a value
        if not self.current_user:
            #If not, redirect him to login page
            self.redirect("/login")
            return
        self.getArgs()
        a = Auth()
        #Check groups of logged in user
        self.groupnames = a.getGroups(self.current_user)
        #Check if selected target user is in accessible group of logged in user
        ans = a.checkUserGroup(self.pairs[0]['user'], self.groupnames)
        #If True - then access granted, show info
        if ans and len(self.pairs) > 0:
            #user.cgi = 3
            ind = 3
            if 'mode' in self.pairs[0]:
                self.genericRender(serverurl + args[0] + '?', ind, self.pairs[0], mode=True)
            else:
                self.genericRender(serverurl + args[0] + '?', ind, self.pairs[0])
        #Access denied
        else:
            self.redirect("/403")

class LoginHandler(BaseHandler):
    def get(self):
        self.write('<html><head><title>Internet usage viewer for Azercell</title></head>'
                   '<body><form action="/login" method="post">'
                   '<div align="center">'
                   '<h1>Internet usage viewer</h1>'
                   'Username: <input type="text" name="username"><br>'
                   'Password: <input type="password" name="password"><br>'
                   '<input type="submit" value="Login">'
                   '<h6>version {0}</h6>'
                   '</div>'
                   '</form></body></html>'.format(version))

    def post(self):
        a = Auth()
        #Check user if authenticated
        res = a.check(self.get_argument("username"), self.get_argument("password"))
        if res:
            self.set_secure_cookie("user", self.get_argument("username"))
            self.redirect("/")
        else:
            self.redirect("/403")

class LogoutHandler(BaseHandler):
    def get(self):
        self.write('<html><head><title>Internet usage viewer for Azercell</title></head>'
                    '<body><form action="/logout" method="post">'
                   '<div align="center">'
                   '<h3>Are you sure you wish to logout?</h3>'
                   '<input type="radio" name="answer" value="1" checked> Yes'
                   '<input type="radio" name="answer" value="0"> No<br>'
                   '<input type="submit" value="Submit">'
                   '<h6>version {0}</h6>'
                   '</div>'
                   '</form></body></html>'.format(version))
    
    def post(self):
        ans = int(self.get_argument("answer"))
        if ans == 1:
            self.clear_cookie("user")
            self.redirect("/exit")
        elif ans == 0:
            self.redirect("/")
        else:
            self.write('Logout error occured')

class ExitHandler(BaseHandler):
    def get(self):
        self.write('<html><head><title>Internet usage viewer for Azercell</title></head>'
                    '<div align="center">'
                    '<h2>Logged out</h2>'
                    '<h2>You may now close your browser tab</h2>'
                    '</div>'
                   '</form></body></html>')

class ErrorHandler(BaseHandler):
    def get(self):
        self.write('<html><body>Access Denied'
                    '</body></html>')
        
app = tornado.web.Application([
    (r"/", MainHandler),
    (r"/login", LoginHandler),
    (r"/logout", LogoutHandler),
    (r"/exit", ExitHandler),
    (r"/403", ErrorHandler),
    (r"/(index.*)", IndexCGIHandler),
    (r"/(group_detail.*)", GroupCGIHandler),
    (r"/(user_detail.*)", UserCGIHandler),
], cookie_secret=base64.b64encode(uuid.uuid4().bytes + uuid.uuid4().bytes))

def main():
    app.listen(4949)
    tornado.ioloop.IOLoop.instance().start()
    return 0

if __name__ == '__main__':
    main()

