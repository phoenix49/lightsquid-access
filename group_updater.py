#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  groupfetcher.py
#  
#  Copyright 2012 Said Babayev <tesaid@azercell.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# Generate new group.cfg and realname.cfg and place it in /etc/lightsquid

import os.path
import sys
import pickle

global startpath
startpath = os.path.dirname(sys.argv[0])

class Main:
    def __init__(self, fname):
        self.fname = fname
        #Try to open group:id file
        try:
            f = open(startpath + '/group.dat')
            self.groups = pickle.load(f)
            f.close()
        except:
            self.groups = {}
        self.groupcfg = []
    
    def run(self):
        f = open(self.fname)
        l = f.readlines()
        for pos, item in enumerate(l):
            item = item.replace('\r\n', '')
            item = item.rstrip()
            item = item.strip('{')
            item = item.strip('}')
            item = item.split(',')
            if len(item) != 3:
                l[pos] = []
            else:
                #strip whitespaces
                clean = []
                for entity in item:
                    clean.append(entity.strip())
                l[pos] = clean
        #find empty vals
        dellist = []
        for pos, item in enumerate(l):
            if len(item) == 0:
                dellist.append(pos)
        dellist.reverse()
        #and delete them
        for item in dellist:
            l.pop(item)
        #l is full list
        p = Parser(l, self.groups, self.groupcfg)
        self.groupcfg, self.groups = p.generateGroups()
        self.grcfg = p.groupCfg()
        self.realname = p.realnameCfg()
        self.save()
        self.output()
    
    def output(self):
        #Write realname.cfg and group.cfg
        try:
            f = open('/etc/lightsquid/realname.cfg', 'w')
            f.write(self.realname)
            f.close()
            f = open('/etc/lightsquid/group.cfg', 'w')
            f.write(self.grcfg)
            f.close()
        except:
            print 'Error while writing to file'
    
    def save(self):
        f = open(startpath + '/group.dat', 'w')
        pickle.dump(self.groups, f)
        f.close()

class Parser:
    #Parse list into group.cfg and realname.cfg compatible formats
    def __init__(self, full_list, groups, groupcfg):
        self.l = full_list
        self.groups = groups
        self.groupcfg = groupcfg
    
    def groupCfg(self):
        #output to group.cfg
        output = ''
        for item in self.groupcfg:
            output += '\t'.join(item)
            output += '\n'
        return output
    
    def realnameCfg(self):
        #output to realname.cfg
        output = ''
        for item in self.l:
            output += item[0] + '\t'
            output += item[1] + '\n'
        return output
    
    def generateGroups(self):
        grid = 1
        for item in self.l:
            #check if group exists
            if not item[2] in self.groups.keys():
                #find new group id
                while True:
                    if not grid in self.groups.values():
                        break
                    grid += 1
                self.groups[item[2]] = grid
            #add new item
            newitem = []
            newitem.append(item[0])
            newitem.append(str(self.groups[item[2]]))
            newitem.append(item[2])
            self.groupcfg.append(newitem)
        return self.groupcfg, self.groups

def main():
    if len(sys.argv) != 2:
        print 'Usage: {0} groups_raw'.format(sys.argv[0])
        return 1
    fname = sys.argv[1]
    if not os.path.isfile(fname):
        print 'File not found'
        return 1
    app = Main(fname)
    app.run()
    return 0

if __name__ == '__main__':
    main()

