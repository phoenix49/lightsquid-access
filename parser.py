#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parser.py
#  
#  Copyright 2012 Said Babayev <tesaid@azercell.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import urllib2
import urllib
import tornado.web
import tornado.ioloop

class Opener:
    def fetch(self, url, ind, dic=None, groups=None, mode=None):
        if dic:
            #dic is arguments
            args = urllib.urlencode(dic)
            url += args
        f = urllib2.urlopen(url)
        l = Changer()
        ans = l.replace_index(f.read())
        if ind == 1:
            #If index.cgi
            ans = l.replace_rtable(ans)
            ans = l.replace_daydetail(ans)
        elif ind == 2:
            #if group.cgi
            if not mode:
                ans = l.replace_recurring(ans, 'Top Sites', 'td', 0)
                ans = l.replace_recurring(ans, 'Big Files', 'td', 0)
                ans = l.del_getcgi(ans)
                ans = ans.replace('colspan="7"', 'colspan="6"')
            ans = l.del_rcolumn(ans)
            ans = l.remove_groups(ans, groups, mode)
        elif ind == 3:
            #if user.cgi
            if not mode:
                ans = l.replace_recurring(ans, 'get.cgi', 'tr', 0)
        else:
            ans = 'Index not recognized'
            return ans
        ans = l.addfooter(ans)
        return ans

class Changer:
    def replace_index(self, html):
        #Replace lightsquid links
        html = html.replace('"index.cgi"', '"/"')
        return html
    
    def remove_groups(self, html, groups, mode=False):
        #Remove groups not in [groups]
        #anchor id
        nextid = 0
        while True:
            anchor = html.find('A name', nextid)
            if anchor < 0:
                end = html.find('<!-- End -->', nextid)
                trstart = html[:end].rfind('<tr>')
                trend = html.find('</tr>', trstart)
                html = html[:trstart] + html[trend:]
                break
            grstart = html.find('>', anchor) + 1
            grend = html.find('<', grstart)
            groupname = html[grstart:grend]
            if not groupname in groups:
                #Delete groups out of scope
                sanchor = html[:anchor].rfind('<tr>')
                nextanchor = html.find('A name', grend)
                nextanchorstart = html[:nextanchor].rfind('<tr>')
                html = html[:sanchor] + html[nextanchorstart:]
                nextid = sanchor
            elif not mode:
                #Remove column
                column = '<td></td>'
                colid = html.find(column, grend)
                html = html[:colid] + html[colid+len(column):]
                nextid = grend
            else:
                nextid = grend
        return html
    
    def replace_rtable(self, html):
        html = html.replace('<TD>Top Sites</TD>', '')
        html = html.replace('<TD>Total</TD>', '')
        for i in range(2):
            html = self.replace_recurring(html, 'topsites.cgi', 'TD', 0)
            html = self.replace_recurring(html, 'month_detail.cgi', 'TD', 0)
        return html
        
    def replace_daydetail(self, html):
        #Replace day links with group link to that day
        html = self.replace_recurring(html, 'size="-1">Group', 'TH', 0) #fix not to confuse with rtable
        html = self.replace_recurring(html, 'Oversize', 'TH', 0)
        html = self.replace_recurring(html, 'Total/Average', 'TR', 0)
        dd = 'day_detail.cgi'
        while True:
            dayid = html.find(dd)
            if dayid < 0:
                break
            groupid = html.find('<A HREF="group_detail.cgi', dayid)
            oversizeid = html.find('<A HREF="day_detail.cgi', groupid)
            #replace from bottom to up
            html = self.replace_recurring(html, 'day_detail.cgi', 'TD', oversizeid)
            html = self.replace_recurring(html, 'group_detail.cgi', 'TD', groupid)
            html = html[:dayid] + dd.replace('day', 'group') + html[dayid+len(dd):]
        return html
        
    def replace_recurring(self, html, text, tag, startid):
        #Remove whole tag (TD)
        start = html.find(text, startid)
        starttag = html[:start].rfind('<' + tag)
        end = html.find(tag, start)
        html = html[:starttag] + html[end+len(tag)+1:]
        return html
    
    def addfooter(self, html):
        fid = html.rfind('http://lightsquid.sf.net')
        startid = html.find('</font>', fid)
        html = html[:startid] + '\n<font size="-1"><b><center><a href="https://bitbucket.org/phoenix49/lightsquid-access">LightSquid Access</a> \
            (c) Azercell<br><a href="/logout">Logout</a></center></b></font>' + html[startid:]
        return html
    
    def del_getcgi(self, html):
        text = 'get.cgi'
        html = self.replace_recurring(html, 'Time', 'th', 0)
        while True:
            getid = html.find(text)
            if getid < 0:
                break
            html = self.replace_recurring(html, text, 'td', getid)
        return html
    
    def del_rcolumn(self, html):
        text = '<!-- Rigth Column -->'
        start = html.find(text)
        tableend = html.find('</table>', start)
        end = html.find('</td>', tableend)
        return html[:start] + '<!-- End -->' + html[end+len('</td>'):]

def main():
    
    return 0

if __name__ == '__main__':
    main()

